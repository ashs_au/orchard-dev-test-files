<?php
/*
  Orchard PHP Dev test.201808.v6
  Answers: Ashley Scott - ashs@allocentric.net
  2. insta-gram
  Given any inputted string, assess if the string is a heterogram, isogram or neither.
  Note this solution. treats upper/lowercase chars as identical

  Execute in the shell as: 'php -d display_errors insta-gram.php'

  "HETEROGRAM" if all letters occur exactly once
  "ISOGRAM" if all letters occur an equal number of times, but is not a heterogram
  "NOTAGRAM" if neither a heterogram and an Isogram
  Non alpha characters are ignored

  Approach:
  1) cleanup input: remove unwanted chars (non-alpha chars inc. spaces)
      & homogenize it (single-case)
  2) examine input for empty string
  3) split the string into an array so we can
  4) record the no. occurences of each char
  5) determine the result by deleting all duplicate values in the array, then
      if-else shortcircuiting:

      NOTOGRAM: if the array is longer than 1
      HETEROGRAM if the single array entry equals 1
      ISOGRAM if the single entry is another amount
*/

function gramTester($input) {
  /*
    All our string cleansing in one place: retain spaces & alphanum chars only.
    PHP Regex char class ref:
    http://php.net/manual/en/regexp.reference.character-classes.php
  */
  $input = strtolower(preg_replace("/[^[:alnum:]]/u", '', $input));
  if(!empty($input)) {
    /*
      split input into a char array
    */
    $chars = str_split($input,1);
    /*
      check for adjacent same-characters to determine result
    */
    $frequency = array();  //char frequency stash
    for($i = 0; $i < count($chars); $i++) {
      $key = $chars[$i];
      /*
        record char occurences as they are discovered
      */
      if(!array_key_exists($key, $frequency)) {
        $frequency[$key] = 1;
      } else {
        $frequency[$key] += 1;
      }
    }
    // print_r($frequency);
    /*
      remove all dup. values in array - PHP array_unique ref:
      http://php.net/manual/en/function.array-unique.php &

      extract just the values from a hash - PHP array_values ref:
      http://php.net/manual/en/function.array-values.php
    */
    $total = array_values(array_unique($frequency));
    if(count($total) > 1) {
      return "NOTAGRAM";
    } elseif($total[0] == 1) {
      return "HETEROGRAM";
    } else {
      return "ISOGRAM";
    }
  }

}

$tests = ['uncopyrightable', 'Caucasus', 'authorising', 'qwerty:', 'tty', 'http', 'craft beer', '371', '1010', '717'];

foreach($tests as $test) {
  echo 'The input: "' . $test . '" The result was a(n): ' . gramTester($test) . "\n";
}
