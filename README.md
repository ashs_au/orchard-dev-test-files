Dev challenges for Orchard PHP Developer Test 1 Nov 2018
Ashley Scott: ashs@allocentric.net

---
## Section 1: CMS
work was done on Drupal 8.5.6 on shared hosting w. PHP 7.1.14

The theme folder with readme is located in this repo under the 'Drupal' directory.

The theme is active at: [http://explodinghare.com](http://explodinghare.com)

I will provide a login via email

---
## Section 2: Logic Questions
This work was created and tested under PHP 7.1.19 on OSX.

1. emordnilap.php
2. insta-gram.php &
5. snap-crackle-pop.php

These are realised as CLI scripts to be run in a terminal
