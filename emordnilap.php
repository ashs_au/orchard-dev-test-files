<?php
/*
  Orchard PHP Dev test.201808.v6
  Answers: Ashley Scott - ashs@allocentric.net
  1. emordnilaP
  Given any inputted string, determine if the string is a palindrome

  Execute in the shell as: 'php -d display_errors emordnilap.php'

  Return "TRUE" if the characters read the same backward as forward
  Return "FALSE" if the characters read differently backward as forward
  "UNDETERMINED" for an empty string
  Ignore non-alphanumeric characters, case-insensitive

  Approach:
  1) cleanup input: remove unwanted chars (non-alpha chars inc. spaces)
      & homogenize it (single-case)
  2) examine input for empty string
  3) simple-compare the original and reversed copy of each, return the verdict
*/

function palindromeTester($input) {
  /*
    All our string cleansing in one place: retain spaces & alpha chars only.
    PHP Regex char class ref:
    http://php.net/manual/en/regexp.reference.character-classes.php
  */
  $input = strtolower(preg_replace("/[^[:alpha:]]/u", '', $input));
  if(!empty($input)) {
    $tupni = strrev($input); // reversed copy

    if($input == $tupni) {
      return "TRUE";
    }
    return "FALSE";
  }
  return "UNDETERMINED";
}

$tests = ['emordnilaP', '', 'kayak', '1No Lemon, 3 No melon1', 'A nut for a jar of Tuna', '78salmon'];

foreach($tests as $test) {
  echo 'The input: "' . $test . '" The result was: ' . palindromeTester($test) . "\n";
}
