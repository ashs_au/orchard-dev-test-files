Section 1 CMS:

I've attached the 'ex' theme folder which extends the standard Drupal 8 Bootstrap 3 theme to this repository. The demo site is up at: http://explodinghare.com. I will provide cerds for an admin login via email.

My intent is to use a segment of the url path to activate the switch between banner images by injecting a class into the html, which will reference the relevant background image in that div.

components:

a. I've created a path pattern ([node:menu-link:parents:join-path]/[node:title])for page content types using the path_auto module that sets the path alias for these to reflect their assigned menu hierarchy. ie '/root-b/sub1'.

b. A preprocess function in the theme file (ex.theme) gets the path of the current node and splits it down to the initial segment. ie, from '/root-b/sub3' to 'root-b' which is then passed to the page template responsible for rendering that area of the page, via the twig template 'page-title.html.twig' 

ISSUE 

Unfortunately: 
A. my understanding of Drupal path aliases was incorrect
B. there's an issue either in Drupal 8.5.6, a module or 
C. this particular hosting/installation 
whereby the path alias of a page doesn't update when it's moved between the top-level menus. So the effective path for Sub3 remains '/root-c/sub-3' when it's moved - never updating to '/root-b/sub-3' and the banner image shown doesn't reflect the new position.

This behaviour persists even after installing the 'Pathauto Menu Link' helper module = which is meant to ensure paths are updated when items are moved but this has had no effect either.

The only workaround at this point is to edit the node's path alias in Administration > Configuration > Search and metadata > URL aliases after moving the menu link

I look forward to moving this theme & content items to a different Drupal version & host to start to identify the cause.