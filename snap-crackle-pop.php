<?php
/*
  Orchard PHP Dev test.201808.v6
  Answers: Ashley Scott - ashs@allocentric.net
  5. Snap, Crackle, Pop

  No input is required. On runtime, iterate through the numbers 1 to 100 and
  output “SNAP”, “CRACKLE”, “POP”, or the number.

  Conditions
  "SNAP" if the number is divisible by 3
  "CRACKLE" if the number is divisible by 5
  "POP" if the number is divisible by both 3 AND 5
  "i" where i is the current number, but only if none of the above conditions
  match
  Output is in the form of a single string, no spaces, no carriage returns

  Approach:
  each iteration of the loop submits to the fizzbuzz function to check the above
  conditions & output accordingly.

*/

function fizzbuzz($input) {
    if((($input % 3) == 0) && (($input % 5) == 0)) { $t = 'POP'; return $t;}
    if(($input % 3) == 0) { $t = 'SNAP'; return $t;}
    if(($input % 5) == 0) { $t = 'CRACKLE'; return $t;}
    if(empty($t)) { $t = $input; return $t;}
}

for($i = 1; $i <= 100; $i++) {
  $out = fizzbuzz($i);
  echo "$out";
}
